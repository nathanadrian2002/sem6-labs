import fileinput

for line in fileinput.input():

	data = line.strip().split(',')
	if len(data) == 14:

		age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal,target= data

		if (target.lower() == 'yes'):
			print(f"{int(age)//10 * 10}")
