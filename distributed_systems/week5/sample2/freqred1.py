
import sys

lastWord = None

s = 0

for line in sys.stdin:

	word, count = line.strip().split('\t', 1)
	count = int(count)

	if lastWord == None:
		lastWord = word 
		s = count
		continue

	if word == lastWord:
		s += count

	else:
		print("%s\t%d" % (lastWord, s))
		s = count 
		lastWord = word 

if lastWord == word:
	print('%s\t%s' % (lastWord, s))