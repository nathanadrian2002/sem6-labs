import numpy as np

def make_matrix():
	n = int(input("Size: "))
	mat = []
	for r in range(n):
		mat.append([])
		for c in range(n):
			mat[r].append(float(input(f"E({[r,c]}): ")))

	return np.array(mat)

a = make_matrix()
b = make_matrix()

if a.shape[0] != b.shape[0]:
	print("Dissimilar shapes")

print("Result: ")
print(a+b)