import numpy as np

def make_matrix():
	n = int(input("Size: "))
	mat = []
	for r in range(n):
		mat.append([])
		for c in range(n):
			mat[r].append(float(input(f"E({[r,c]}): ")))

	return np.array(mat)

mat = make_matrix()
print("Original: ")
print(mat)

print("Transpose: ")
print(mat.transpose())