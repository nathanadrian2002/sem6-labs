import numpy as np

mat = np.array([[1, 2, 3],
		  [4, 5, 6],
		  [7, 8, 9]])

print(mat)
for r_no in range(len(mat)):
	# print(mat[r_no, :])
	print("Row", r_no ,"Sum: ", sum(mat[r_no, :]))

for c_no in range(len(mat[0])):
	print("Col", c_no,"Sum: ", sum(mat[:, c_no]))	