import numpy as np   


l = [1,2, 3, 4]

fa = np.array(l, dtype= np.float64)
print(fa.dtype)

t = (5 , 6, 7, 8)

a = np.array(t)
print(type(a))


z = np.zeros((3,4))
print(z.shape)

s = np.arange(0, 21 ,5)
print(s)

print(z.reshape((2, 2, 3)).shape)


mat = np.array([[1, 2, 3],
		  [4, 5, 6],
		  [7, 8, 9]])

print("Max: ", max(mat.ravel()))
print("Min: ", min(mat.ravel()))
print("Sum: ", sum(mat.ravel()))

for rn in range(len(mat)):
	print(f"R{rn} max: {max(mat[rn, :])}")
	print(f"R{rn} min: {min(mat[rn, :])}")

for cn in range(len(mat[0])):
	print(f"C{cn} max: {max(mat[:, cn])}")
	print(f"C{cn} min: {min(mat[:, cn])}")

