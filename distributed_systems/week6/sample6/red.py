
import fileinput

highest_conf = {}


for line in fileinput.input():

	data = line.strip().split("\t")

	if len(data) != 2:
		continue

	region, confirmed = data

	try:
		confirmed = int(confirmed)
	except ValueError:
		continue

	highest_conf[region] = max(confirmed, highest_conf.get(region, 0))

for key, value in highest_conf.items():
	print(f"{key}\t{value}")