import fileinput

for line in fileinput.input():

	data = line.strip().split(',')
	if len(data) == 8:

		n,obs_date,state, region, last_update, confirmed, deaths,recovered = data

		print(f"{region}\t{confirmed}")
