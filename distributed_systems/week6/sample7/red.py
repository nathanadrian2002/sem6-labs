
import sys

cur_num = None

cur_count = 0

num = None

for line in sys.stdin:

	num, count = line.strip().split('\t',1)

	try:
		count = int(count)

	except ValueError:
		print(f"Quiting {num}")
		continue

	if cur_num == num:
		cur_count += count 


	else:
		if cur_num:
			print(f"{cur_num}\t{cur_count}")

		cur_count = count
		cur_num = num 

if cur_num == num:
	print(f'{cur_num}\t{cur_count}')