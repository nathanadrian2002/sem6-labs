import pandas as pd
import numpy as np


dic = {
	'Name': ['Annie', 'Diya', 'Charles', 'James', 'Emily'],
	'Quiz_1/10': [8.0, 9, 7.5, 8.5, 6.5],
	'In-Sem_1/15': [11.0, 14,14.5,13, 12.5],
	'Quiz_2/10': [9.5, 6.5, 8.5, 9, 9],
	'In-Sem_2': [12.5, 13.5, 14.5, 15.0, 13]
}


df = pd.DataFrame(dic)

df.insert(len(dic), 'Total', [df.iloc[x, 1:].sum(axis=0) for x in range(len(df))])
df.loc['mean'] = df.mean(numeric_only = True)
print(df)


#          Name  Quiz_1/10  In-Sem_1/15  Quiz_2/10  In-Sem_2  Total
# 0       Annie        8.0         11.0        9.5      12.5   41.0
# 1        Diya        9.0         14.0        6.5      13.5   43.0
# 2     Charles        7.5         14.5        8.5      14.5   45.0
# 3       James        8.5         13.0        9.0      15.0   45.5
# 4       Emily        6.5         12.5        9.0      13.0   41.0
# mean      NaN        7.9         13.0        8.5      13.7   43.1