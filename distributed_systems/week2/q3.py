import pandas as pd
import numpy as np

dic = {	'Name': ['Raul', 'Sam', 'Tom'],
		'Height': [150, 160, 170],
		'Qualification': ['Masters', 'PHD', 'HighSchool']
}


df = pd.DataFrame(dic)

print(df)

address = ['A', 'B', 'C']

df['address'] = address

print(df)


df.insert(1, 'Insert', [5, 8, 3])

print(df)
