import pandas as pd
import numpy as np


df1 = pd.DataFrame(['Ram', 'Diya', 'Chandan', 'James', 'Alice'], columns=['Name'])

df2 = [ [80.0, 81.0, 91.5, 82.5], 
		[90.0, 84, 86.5, 83.5],
		[77.5, 74.5, 85.5, 84.5],  
		[87.5, 83, 90, 85],
		[86.5, 82.5, 91, 93] ]

df2 = pd.DataFrame(df2, columns=['Maths', 'Physics', 'Chemistry', 'Biology'])

print(df1)
print(df2)

df3 = pd.concat([df1, df2], axis = 1)
# print(df3)

df3['Total'] = [df3.iloc[x, 1:].sum(axis=0) for x in range(len(df3))]

print(df3)


#       Name
# 0      Ram
# 1     Diya
# 2  Chandan
# 3    James
# 4    Alice
#    Maths  Physics  Chemistry  Biology
# 0   80.0     81.0       91.5     82.5
# 1   90.0     84.0       86.5     83.5
# 2   77.5     74.5       85.5     84.5
# 3   87.5     83.0       90.0     85.0
# 4   86.5     82.5       91.0     93.0
#       Name  Maths  Physics  Chemistry  Biology  Total
# 0      Ram   80.0     81.0       91.5     82.5  335.0
# 1     Diya   90.0     84.0       86.5     83.5  344.0
# 2  Chandan   77.5     74.5       85.5     84.5  322.0
# 3    James   87.5     83.0       90.0     85.0  345.5
# 4    Alice   86.5     82.5       91.0     93.0  353.0
