import socket

# create a socket object
sock = socket.socket(
socket.AF_INET, socket.SOCK_DGRAM)

# get local machine name
host = socket.gethostname()
port = 9991

sock.sendto("Time?".encode(), (host, port))

dt = sock.recvfrom(1024)[0].decode()

print(dt)