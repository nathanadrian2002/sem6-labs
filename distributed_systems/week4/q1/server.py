import socket
import time

# create a socket object
sock = socket.socket(
socket.AF_INET, socket.SOCK_DGRAM)

# get local machine name
host = socket.gethostname()
port = 9991

# bind to the port
sock.bind((host, port))

while True:
# establish a connection
	_,addr = sock.recvfrom(1024)
	print("Got a connection from %s" % str(addr))
	ct = time.ctime(time.time()) + "\r\n"
	sock.sendto(ct.encode(), addr)		