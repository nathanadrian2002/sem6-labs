import socket

# create a socket object
serversocket = socket.socket(
socket.AF_INET, socket.SOCK_DGRAM)

# get local machine name
host = socket.gethostname()
port = 9991

# bind to the port
serversocket.bind((host, port))

while True:
# establish a connection
	name, addr = serversocket.recvfrom(1024)
	print("Connection with", name)

	msg, _ = serversocket.recvfrom(1024)
	while msg:

		print(f"{name.decode()}: {msg.decode()}")

		msg = input("Me> ")

		serversocket.sendto(msg.encode(), addr)

		if not msg:
			break

		msg, _ = serversocket.recvfrom(1024)
	print("Chat Ended")
