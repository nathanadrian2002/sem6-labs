import socket

# create a socket object
sock = socket.socket(
socket.AF_INET, socket.SOCK_DGRAM)

# get local machine name
host = socket.gethostname()
port = 9991

name = input("Enter your name: ") 

sock.sendto(name.encode(), (host,port))

msg = input("Me> ")

while msg:

	sock.sendto(msg.encode(), (host,port))
	
	msg, _ = sock.recvfrom(1024)

	if not msg:
		break

	print("Server>", msg.decode())

	msg = input("Me> ")


print("Chat Ended")