
a = int(input("Enter a number: "))
b = int(input("Enter a number: "))
c = int(input("Enter a number: "))


if (a > b and a > c):
	print(f"A: {a} is the largest")

elif (b > a and b > c):
	print(f"B: {b} is the largest")

else:
	print(f"C: {c} is the largest")

# Output: 

# Enter a number: 5
# Enter a number: 2
# Enter a number: 7
# C: 7 is the largest

# Enter a number: 7
# Enter a number: 6
# Enter a number: 5
# A: 7 is the largest

# Enter a number: 9
# Enter a number: 3
# Enter a number: 2
# A: 9 is the largest
