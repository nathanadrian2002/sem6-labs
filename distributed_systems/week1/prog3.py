 
x = int(input("Enter a number: "))

print("Odd") if x % 2 else print("Even")

# Output:

# Enter a number: 7
# Odd

# Enter a number: 2
# Even
