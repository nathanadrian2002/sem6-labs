
a = 5
b = 6
print(f"{a = } {b = }")
print("Swapping")
a, b = b, a

print(f"{a = } {b = }")

# Output:
# a = 5 b = 6
# Swapping
# a = 6 b = 5
