
t = (1,3,5,7,9,2,4,6,8,10)

print(t[:int(len(t)/2)])
print(t[int(len(t)/2):])

# Output:

# (1, 3, 5, 7, 9)
# (2, 4, 6, 8, 10)
