

l = [1,3,5,7,9,2,4,6,8,10]
temp = l.copy()

for i in l:

	if (i % 2 == 0): temp.remove(i)

l = temp
print(l)

# Output:
# [1, 3, 5, 7, 9]
