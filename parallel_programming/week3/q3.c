#include <mpi.h>
#include <stdio.h>
#include <string.h>

int mc = MPI_COMM_WORLD;


int main(int argc, char* argv[]){
	int r,s;
	int co = 0;
	int b[100] = {0};
	int i, n, l;
	char str[100], c[100];
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(mc, &r);
	MPI_Comm_size(mc, &s);

	if (r == 0){
		n = s;
		printf("Enter string: ");
		scanf("%s", str);
		l = strlen(str) / n;
	}

	MPI_Bcast(&l, 1, MPI_INT, 0, mc);
	MPI_Scatter(str, l, MPI_CHAR, c, l, MPI_CHAR, 0, mc);
	co = 0;

	for (i = 0; i < l; ++i){
		if(c[i] =='a' || c[i] == 'e' || c[i] == 'i' || c[i] == 'o' || c[i] == 'u')
			continue;
		co+=1;
	}

	printf("Process %d Count = %d\n", r, co);
	fflush(stdout);
	MPI_Gather(&co, 1, MPI_INT, b, 1, MPI_INT, 0, mc);

	if (r == 0){
		int tcount = 0;
		for (i = 0; i < n; i++)
			tcount += b[i];
		printf("Total non vowels = %d\n", tcount);
		fflush(stdout);
	}
	MPI_Finalize();
}
