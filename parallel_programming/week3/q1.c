#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int mc = MPI_COMM_WORLD;
int l = MPI_LONG;

void main (int argc, char * argv[]) {
    int r, s;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(mc, &r);
    MPI_Comm_size(mc, &s);
    MPI_Barrier(mc);
    
    long fact;
    long i, n;
    long rec;
    long arr[100], facts[100];

    if (r == 0){
        n = s;
        printf("Enter the numbers: ");
        for (i = 0; i < n; ++i) 
            scanf("%ld", &arr[i]);

    }
    double sst = MPI_Wtime();
    MPI_Scatter(arr, 1, l, &rec, 1, l, 0, mc);
    printf("Process [%d] received = %ld.\n", r, rec);

    fact = 1;
    for (i = 2; i <= rec; ++i) 
        fact *= i;
    MPI_Gather(&fact, 1, l, facts, 1, l, 0, mc);

    if (r == 0){
        printf("Sum of factorials = ");
        long sum = 0;
        
        for (i = 0; i < n; ++i) {
            sum += facts[i];
            // printf("%ld %s", facts[i], (i != n-1)?"+ ":" ");
        }
                                                                                  
        double eet = MPI_Wtime();
        printf(" = %ld\n", sum);
        printf("Time: %f\n",(eet-sst));
    }
    MPI_Finalize();
}