#include<stdio.h>
#include<mpi.h>
#include<stdlib.h>



int mc = MPI_COMM_WORLD;

int main(int argc, char* argv[]){
	int r, s, arr[50], recv[20], n;
	float avg, sum;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(mc,&r);
	MPI_Comm_size(mc,&s);

	if(r == 0){
		printf("Enter a number : ");
		scanf("%d",&n);

		printf("Enter %d elements : ",n*s);
		for(int i=0;i<n*s;i++)
			scanf("%d",&arr[i]);
	}

	MPI_Bcast(&n,1,MPI_INT,0,mc);
	MPI_Scatter(arr,n,MPI_INT,recv,n,MPI_INT,0,mc);

	for(int i=0;i<n;i++)
		avg += recv[i];
	avg /= n;
	printf("Avg value in P%d: %f.\n",r,avg);
	MPI_Reduce(&avg,&sum,1,MPI_FLOAT,MPI_SUM,0,mc);
	sum /= s;

	if(r == 0)
		printf("Total avg value: %f.\n",sum);
	MPI_Finalize();
}


