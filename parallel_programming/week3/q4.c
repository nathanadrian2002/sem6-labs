#include <mpi.h>
#include <stdio.h>
#include <string.h>

int mc = MPI_COMM_WORLD, ch = MPI_CHAR;

int main(int argc, char* argv [])
{

	int r, s;
	float avg = 0;
	char b[100], str1[100], str2[100], c1[100], c2[100], co[100];
	int i, j, m;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(mc, &r);
	MPI_Comm_size(mc, &s);
	
	if (r == 0){ 
		printf("Enter string 1: ");
		scanf("%s", str1);

		printf("Enter string 2: ");
		scanf("%s", str2);
		m = strlen(str1) / s;
	}
	
	MPI_Bcast(&m, 1, MPI_INT, 0, mc);
	MPI_Scatter(str1, m, ch, c1, m, ch, 0, mc);
	MPI_Scatter(str2, m, ch, c2, m, ch, 0, mc);
	
	int t = 0;
	for (t = 0; t <= 2 * m; t += 2){
		co[t] = c1[t/2];
		co[t+1] = c2[t/2];
	}
	
	co[2*m] = '\0';
	MPI_Gather(co, 2*m, ch, b, 2*m, ch, 0, mc);
	
	if (r == 0){
		b[m*s*2] = '\0';
		printf("Result: %s\n",b);
	}
	MPI_Finalize();

}