#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

__global__ void multiply(int *a, int *b, int *c, int n, int m, int k)
{
    int col = blockIdx.x;
    for (int row = 0; row < n; row++)
    {
        int sum = 0;
        for (int i = 0; i < m; i++)
        {
            sum += a[row * m + i] * b[i * k + col];
        }
        c[row * k + col] = sum;
    }
}


int main()
{
    int *a, *b, *c;
    int *d_a, *d_b, *d_c;
    int n, m, k;

    printf("Dims A: ");
    scanf("%d%d", &n, &m);
    printf("Dims B: ");
    scanf("%d%d", &m, &k);

    int size_a = n * m * sizeof(int);
    int size_b = m * k * sizeof(int);
    int size_c = n * k * sizeof(int);
    a = (int *)malloc(size_a);
    b = (int *)malloc(size_b);
    c = (int *)malloc(size_c);
    cudaMalloc(&d_a, size_a);
    cudaMalloc(&d_b, size_b);
    cudaMalloc(&d_c, size_c);

    printf("Matrix A: \n");
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            scanf("%d", &a[i * m + j]);
        }
    }
    printf("Matrix B: \n");
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < k; j++)
        {
            scanf("%d", &b[i * k + j]);
        }
    }
    cudaMemcpy(d_a, a, size_a, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, size_b, cudaMemcpyHostToDevice);

    dim3 grid_size_row(n, 1, 1);

    dim3 grid_size_col(k, 1, 1);
    dim3 block_size_col(1, 1, 1);
    dim3 grid_size_elem((n * k + 255) / 256, 1, 1);




    multiply<<<grid_size_col, block_size_col>>>(d_a, d_b, d_c, n, m, k);
    cudaMemcpy(c, d_c, size_c, cudaMemcpyDeviceToHost);
    printf("Result col wise: \n");
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < k; j++)
        {
            printf("%d ", c[i * k + j]);
        }
        printf("\n");
    }
    printf("\n");




    free(a);
    free(b);
    free(c);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    return 0;
}