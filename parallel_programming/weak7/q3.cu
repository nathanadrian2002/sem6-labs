#include<stdio.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


__global__ void Conv1D(float *N, float *M,float *P,int as,int ms)
{
	int tid=blockIdx.x*blockDim.x+threadIdx.x;
	float pValue=0;
	int startPoint= tid - (ms/2);

	for(int j=0;j<ms;j++)
	{
		if((startPoint+j>=0)&&(startPoint+j<as))
			pValue+=N[startPoint+j]*M[j];
	}
	P[tid]=pValue;
}

int main(void) 
{
	int as,ms;

	printf("Array Size: ");
	scanf("%d",&as);

	printf("Mask Size: ");
	scanf("%d",&ms);

	float *h_N,*h_P,*h_M;
	float *d_N,*d_P,*d_M;

	h_N=(float*)malloc(sizeof(float)*as);
	h_P=(float*)malloc(sizeof(float)*as);
	h_M=(float*)malloc(sizeof(float)*ms);

	cudaMalloc((void **)&d_N,sizeof(float)*as);
	cudaMalloc((void **)&d_P,sizeof(float)*as);
	cudaMalloc((void **)&d_M,sizeof(float)*ms);

	printf("Array: ");
	for(int i=0;i<as;i++)
		scanf("%f",&h_N[i]);

	printf("Mask: ");
	for(int i=0;i<ms;i++)
		scanf("%f",&h_M[i]);


	cudaMemcpy(d_N, h_N,sizeof(float)*as, cudaMemcpyHostToDevice);
	cudaMemcpy(d_M, h_M,sizeof(float)*ms, cudaMemcpyHostToDevice);
	
	dim3 dimGrid(ceil(as/256.0),1,1);
	dim3 dimBlock(256,1,1);

	Conv1D<<<dimGrid,dimBlock>>>(d_N,d_M,d_P,as,ms);
	
	cudaMemcpy(h_P, d_P,sizeof(float)*as, cudaMemcpyDeviceToHost);

	printf("Result: ");
	for(int i=0;i<as;i++)
		printf("%f  ",h_P[i]);

	printf("\n");
	cudaFree(d_N);
	cudaFree(d_P);
	cudaFree(d_M);
	return 0;
}
