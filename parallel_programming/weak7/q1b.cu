#include<stdio.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"



__global__ void vectorAdd(float *A, float *B,float *C)
{
	int tid=threadIdx.x;
	C[tid]=A[tid]+B[tid];
}


int main(void) 
{
	int n;

	printf("N: ");
	scanf("%d",&n);

	float *h_A,*h_B,*h_C;
	float *d_A, *d_B, *d_C;
	
	int size = sizeof(float)*n;

	h_A=(float*)malloc(sizeof(size));
	h_B=(float*)malloc(sizeof(size));
	h_C=(float*)malloc(sizeof(size));

	cudaMalloc((void **)&d_A, size);
	cudaMalloc((void **)&d_B, size);
	cudaMalloc((void **)&d_C, size);

	printf("V1: ");
	for(int i=0;i<n;i++)
		scanf("%f",&h_A[i]);

	printf("V2: ");
	for(int i=0;i<n;i++)
		scanf("%f",&h_B[i]);


	cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);
	

	dim3 dimGrid(1,1,1);
	dim3 dimBlock(n,1,1);

	vectorAdd<<<dimGrid,dimBlock>>>(d_A,d_B,d_C);
	
	cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);

	printf("Result: ");
	for(int i=0;i<n;i++)
		printf("%f  ",h_C[i]);

	printf("\n");

	return 0;
}
