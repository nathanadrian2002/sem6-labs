#include<stdio.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


__global__ void radToSin(float *A, float *B,int n)
{
	int tid=blockIdx.x*blockDim.x+threadIdx.x;

	if(tid<n)
		B[tid]=sin(A[tid]);
}

int main(void) 
{
	int n;

	printf("N: ");
	scanf("%d",&n);

	float *h_A,*h_B;
	float *d_A, *d_B;
	
	int s = sizeof(float)*n;

	h_A=(float*)malloc(sizeof(s));
	h_B=(float*)malloc(sizeof(s));

	cudaMalloc((void **)&d_A, s);
	cudaMalloc((void **)&d_B, s);

	printf("V: ");
	for(int i=0;i<n;i++)
		scanf("%f",&h_A[i]);

	cudaMemcpy(d_A, h_A, s, cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, h_B, s, cudaMemcpyHostToDevice);
	
	dim3 dimGrid(ceil(n/256.0),1,1);
	dim3 dimBlock(256,1,1);

	radToSin<<<dimGrid,dimBlock>>>(d_A,d_B,n);
	
	cudaMemcpy(h_B, d_B, s, cudaMemcpyDeviceToHost);

	printf("Result: ");
	for(int i=0;i<n;i++)
		printf("%f\t",h_B[i]);

	printf("\n");

	cudaFree(d_A);
	cudaFree(d_B);
	return 0;
}
