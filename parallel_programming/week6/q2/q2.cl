__kernel void parallel_selection_sort(__global int *input,__global int *output)
{
	int p = 0;
	int tid = get_global_id(0);
	int d = input[tid];
	int n = get_global_size(0);

	for(int i=0;i<n;i++)
	{
		if((input[i]<d)||(input[i]==d && i<tid))
			p++;
	}
	output[p]=d;
}