// A COMPLETE PROGRAM FOR VECTOR-VECTOR ADDITION
#include <stdio.h>
#include <CL/cl.h>
#include<stdlib.h>

//Max source size of the ke string
#define MAX_SOURCE_SIZE (0x100000)

int main(void)
{
	// Create the two input vectors
	int i;
	int n;
	printf("N: ");
	scanf("%d",&n);
	
	int *A = (int*) malloc (sizeof (int) * n);
	printf("Elements: ");
	//Initialize the input vectors
	for(i = 0; i < n; i++)
	{
		
		// A[i] = n-i; //if n is very large
		scanf("%d", A+i);
	}
	
	// Load the ke source code into the array s_str
	FILE *fp;
	char *s_str;
	size_t s_size;
	fp = fopen("q2.cl", "r");
	if (!fp)
	{
		fprintf(stderr, "Failed to load ke.\n");
		getchar();
		exit(1);
	}

	s_str = (char*)malloc(MAX_SOURCE_SIZE);
	s_size = fread( s_str, 1, MAX_SOURCE_SIZE, fp);
	fclose( fp );

	// Get platform and device information
	cl_platform_id plat_id = NULL;
	cl_device_id dev_id = NULL;
	cl_uint num_dev;
	cl_uint num_plat;
	cl_int ret = clGetPlatformIDs(1, &plat_id, &num_plat);
	ret = clGetDeviceIDs( plat_id, CL_DEVICE_TYPE_GPU, 1,&dev_id,&num_dev);

	// Create an OpenCL con
	cl_context con = clCreateContext( NULL, 1, &dev_id, NULL, NULL, &ret);
	
	// Create a command queue
	cl_command_queue cq = clCreateCommandQueue(con,dev_id,0,&ret);
	
	// Create memory buffers on the device for each vector A, B and C
	cl_mem a_mem_obj = clCreateBuffer(con, CL_MEM_READ_ONLY,n *sizeof(int), NULL, &ret);
	cl_mem b_mem_obj = clCreateBuffer(con, CL_MEM_WRITE_ONLY,n *sizeof(int), NULL, &ret);
	
	// Copy the lists A and B to their respective memory buffers
	ret = clEnqueueWriteBuffer(cq, a_mem_obj, CL_TRUE, 0,n *sizeof(int), A, 0, NULL, NULL);
	
	// Create a pro from the ke source
	cl_program pro = clCreateProgramWithSource(con, 1, (const char**)&s_str, (const size_t *)&s_size, &ret);
	
	// Build the pro
	ret = clBuildProgram(pro, 1, &dev_id, NULL, NULL, NULL);
	
	// Create the OpenCL ke object
	cl_kernel ke = clCreateKernel(pro, "parallel_selection_sort", &ret);
	
	// Set the arguments of the ke
	ret = clSetKernelArg(ke, 0, sizeof(cl_mem), (void *)&a_mem_obj);
	ret = clSetKernelArg(ke, 1, sizeof(cl_mem), (void *)&b_mem_obj);
	
	
	// Execute the OpenCL ke on the array
	size_t global_item_size = n;
	size_t local_item_size = 1;
	
	//Execute the ke on the device
	cl_event event;
	ret = clEnqueueNDRangeKernel(cq, ke, 1, NULL, &global_item_size,&local_item_size, 0, NULL, NULL);
	ret = clFinish(cq);
	
	// Read the memory buffer C on the device to the local variable C
	int *B = (int*)malloc(sizeof(int)*n);
	ret = clEnqueueReadBuffer(cq, b_mem_obj, CL_TRUE, 0,n *sizeof(int), B, 0, NULL, NULL);
	
	// Display the result to the screen

	printf("Input array:\n");
	for(i = 0; i < n; i++)
		printf("%d  ",A[i]);
		
	printf("\nResultant array:\n");
	for(i = 0; i < n; i++)
		printf("%d  ",B[i]);
		
	printf("\n");
	
	// Clean up
	ret = clFlush(cq);
	ret = clReleaseKernel(ke);
	ret = clReleaseProgram(pro);
	ret = clReleaseMemObject(a_mem_obj);
	ret = clReleaseMemObject(b_mem_obj); 
	ret = clReleaseCommandQueue(cq); 
	ret = clReleaseContext(con);
	free(A);
	free(B);
	
	getchar();
	return 0;
}