__kernel void copy_string(__global char *str, int len,__global char *result)
{
	int tid= get_global_id(0);

	for(int i=0;i<len;i++)
		result[len*tid+i]=str[i];
}