#include <stdio.h>
#include<time.h>
#include <CL/cl.h>
#include<string.h>
#include<stdlib.h>

#define MAX_SOURCE_SIZE (0x100000)
int main(void)
{
    char str[200];
    char *res;
    int n;
//Initialize the input string
    int i;
    printf("Input String: ");
    gets(str);
    int len= strlen(str);
    printf("Enter n: ");
    scanf("%d",&n);

    res = malloc(sizeof(char)*len*n);
   
    
// Load the ker source code into the array source_str
    FILE *fp;
    char *source_str;
    size_t source_size;
    fp = fopen("q1.cl", "r");
    if (!fp)
    {
        fprintf(stderr, "Failed to load ker.\n");
        getchar();
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread( source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose( fp );
    cl_platform_id plat_id = NULL;
    cl_device_id dev_id = NULL;
    cl_uint num_devs;
    cl_uint num_plats;

    cl_int ret = clGetPlatformIDs(1, &plat_id, &num_plats);
    
    ret = clGetDeviceIDs( plat_id, CL_DEVICE_TYPE_GPU, 1,&dev_id,&num_devs);

// Create an OpenCL con
    cl_context con = clCreateContext( NULL, 1, &dev_id, NULL, NULL, &ret);

// Create a command queue
    cl_command_queue cmdq = clCreateCommandQueue(con,dev_id, CL_QUEUE_PROFILING_ENABLE, &ret);

// Create memory buffers on the device for input and output string  
    cl_mem s_mem_obj = clCreateBuffer(con, CL_MEM_READ_ONLY,len*sizeof(char), NULL, &ret);
    cl_mem res_mem_obj = clCreateBuffer(con, CL_MEM_WRITE_ONLY,len*sizeof(char)*n, NULL, &ret);

// Copy the input string into respective memory buffer
    ret = clEnqueueWriteBuffer(cmdq, s_mem_obj, CL_TRUE, 0,len *sizeof(char), str, 0, NULL, NULL);

// Create a pro from the ker source
    cl_program pro = clCreateProgramWithSource(con, 1,(const char**)&source_str, (const size_t *)&source_size, &ret);

// Build the pro
    ret = clBuildProgram(pro, 1, &dev_id, NULL, NULL, NULL);

// Create the OpenCL ker
    cl_kernel ker = clCreateKernel(pro, "copy_string", &ret);

// Set the arguments of the ker
    ret = clSetKernelArg(ker, 0, sizeof(cl_mem), (void *)&s_mem_obj);
    ret = clSetKernelArg(ker, 1,sizeof(int),&len);
    ret = clSetKernelArg(ker, 2, sizeof(cl_mem), (void *)&res_mem_obj);

// Set the global work size as string length
    size_t gis = n; // Process the entire lists
    size_t lis =1;

//Execute the OpenCL ker for entire string in parallel
    ret = clEnqueueNDRangeKernel(cmdq, ker, 1, NULL,&gis, &lis, 0, NULL,NULL);
  
// Read the result in memory buffer on the device to the local variable strres
    ret = clEnqueueReadBuffer(cmdq, res_mem_obj, CL_TRUE, 0,len*sizeof(char)*n,res, 0, NULL, NULL);
    res[len*n]='\0';
    printf("Resultant string :%s\n",res);
    getchar(); 

    ret = clReleaseKernel(ker);
    ret = clReleaseProgram(pro);
    ret = clReleaseMemObject(s_mem_obj);
    ret = clReleaseMemObject(res_mem_obj);
    ret = clReleaseCommandQueue(cmdq);
    ret = clReleaseContext(con);
    free(res);
    return 0;
}
