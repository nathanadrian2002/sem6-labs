#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>
#include <time.h>
#include <string.h>
// #include<conio.h>

#define MAX_SOURCE_SIZE (0x100000)

int main() {

    time_t start, end;

    start = clock();

    char tempstr[20486];

    int i;

    // for (i = 0; i < 20485; i++)
    //     tempstr[i] = 'A';
    // tempstr[20485] = '\0';

    printf("Enter string: ");
    scanf("%s", tempstr);

    int len = strlen(tempstr);

    len++;

    char * str = (char*)malloc(sizeof(char) * len);

    strcpy(str, tempstr);


    FILE *fp;
    char * source_str;
    size_t source_size;

    fp = fopen("strtoggle.cl", "r");

    if (!fp){
        fprintf(stderr, "Failed to load kernel");
        exit(1);
    }

    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    
    fclose(fp);

    // OpenCL variables
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;
    cl_uint ret_num_devices, ret_num_platforms;

    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);

    
    clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, &ret_num_devices);

    cl_context context = clCreateContext(NULL, 1, &device_id, NULL, NULL, NULL);
    cl_command_queue queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, NULL);


    // Create OpenCL buffer
    cl_mem s_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, len * sizeof(char), NULL, &ret);

    ret = clEnqueueWriteBuffer(queue, s_mem_obj, CL_TRUE, 0, len * sizeof(char), str, 0, NULL,NULL);


    cl_program program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t*)&source_size, &ret);

    ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

    cl_kernel kernel = clCreateKernel(program, "str_chgcase", &ret);








    // Set kernel argument
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&s_mem_obj);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&s_mem_obj);


    // Execute the kernel
    size_t global_item_size = len;  
    size_t local_item_size = 1;  


    cl_event event;

    ret = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global_item_size, &local_item_size, 0, NULL, &event);
  
    time_t stime = clock();  

    ret = clFinish(queue);

    cl_ulong time_start, time_end;
    double total_time;


    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);

    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);

    total_time = (double) (time_end - time_start);    


    char * strres = (char*)malloc(sizeof(char) * len);

    // Read the modified array back from the GPU
    ret = clEnqueueReadBuffer(queue, s_mem_obj, CL_TRUE, 0, len * sizeof(char), strres, 0, NULL, NULL);

 
    // Display the modified array
    printf("Done\n");
    strres[len-1] = '\0';

    printf("Toggled String: %s\n",strres);

    getchar();

    // Clean up
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    
    clReleaseMemObject(s_mem_obj);
    // clReleaseMemObject(t_mem_obj);


    clReleaseCommandQueue(queue);
    clReleaseContext(context);


    end = clock();

    printf("Time taken: %0.3f msec\n", total_time /1000000);

    free(str);
    free(strres);
    // getch();
    return 0;
}
