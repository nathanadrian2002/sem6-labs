#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

#define BUFSIZE 2000

int main(int argc, char *argv[])
{
	int rank, size, rec, y;
	int *x;
	int arr[50];
	
	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	MPI_Status status;



	if (rank == 0)
	{
		y = MPI_BSEND_OVERHEAD + sizeof(int) * size;
		// x = calloc(sizeof(int), size);
		x = malloc(y);
		printf("Enter %d values: ", size);
		
		MPI_Buffer_attach(x, y);
		for (int i=0; i < size; i++)
			scanf("%d", arr + i);

		for (int i = 1; i < size; i++)
			MPI_Bsend(arr + i, 1, MPI_INT,i, 1, MPI_COMM_WORLD);

		printf("P0 sent\n");
		fflush(stdout);
		rec = x[0];

		

	}
	else{
		MPI_Recv(&rec, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
		fflush(stdout);
	}


	if (rank %2 == 0)
	{
		printf("P%d: %d\n", rank, rec * rec);
	}
	else
	{
		printf("P%d: %d\n", rank, rec * rec * rec);

	}

	MPI_Buffer_detach(&x, &y);
	MPI_Finalize();

	return 0;
}