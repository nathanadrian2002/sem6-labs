#include<stdio.h>
#include <ctype.h>
#include<string.h>
#include "mpi.h"

int main(int argc, char *argv[])
{
	int rank, size;
	char x[50];

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status status;

	if (rank == 0)
	{
		printf("Enter a string: ");
		scanf("%s", x);

		MPI_Ssend(&x, 50, MPI_CHAR,1, 1, MPI_COMM_WORLD);

		printf("P1: sent %s\n", x);
		fflush(stdout);

		MPI_Recv(&x, 50, MPI_CHAR, 1, 1, MPI_COMM_WORLD, &status);
		printf("P1: recv %s\n", x);
	}
	else{
		MPI_Recv(&x, 50, MPI_CHAR, 0, 1, MPI_COMM_WORLD, &status);
		printf("P2: recv %s\n", x);

		for (int i = 0; i < strlen(x); i++)
		{
			if (isalpha(x[i]))
				x[i] ^= ' ';
		}

		printf("P2: sent %s\n", x);

		fflush(stdout);

		MPI_Ssend(&x, 50, MPI_CHAR,0, 1, MPI_COMM_WORLD);
	}

	MPI_Finalize();

	return 0;
}


// Enter a string: HellO 
// P1: sent HellO
// P1: recv hELLo
// P2: recv HellO
// P2: sent hELLo
