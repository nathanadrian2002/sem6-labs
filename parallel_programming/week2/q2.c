#include<stdio.h>
#include "mpi.h"

int main(int argc, char *argv[])
{
	int rank, size, x;

	int z = MPI_COMM_WORLD;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(z, &rank);
	MPI_Comm_size(z, &size);
	MPI_Status status;

	if (rank == 0)
	{
		printf("Enter a value: ");
		scanf("%d", &x);
		for (int i = 1; i < size; i++)
			MPI_Send(&x, 1, MPI_INT,i, 1, z);

		fprintf(stdout, "P0 sent %d\n", x);
		fflush(stdout);
	}
	else{
		MPI_Recv(&x, 1, MPI_INT, 0, 1, z, &status);
		fprintf(stdout, "P%x recv: %d\n", rank ,x);
		fflush(stdout);
	}

	MPI_Finalize();

	return 0;
}

// Enter a value: 6
// P0 sent 6
// P2 recv: 6
// P3 recv: 6
// P4 recv: 6
// P1 recv: 6
