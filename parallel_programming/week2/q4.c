#include<stdio.h>
#include "mpi.h"

int main(int argc, char *argv[])
{
	int rank, size, x ;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status status;


	if (rank == 0)
	{
		printf("Number: "); scanf("%d", &x);
		fflush(stdout);
	}

	for (int i = 0; i < size; i++)
	{

		// printf("from %d to %d\n", i % x, (i+1) % x);
		if (rank == i % size)
		{
			// printf("P%d waitn to send P%d\n", rank, (i+1) % size);
			MPI_Send(&x, 1, MPI_INT, (i+1) % size, 1, MPI_COMM_WORLD);
			printf("P%d send %d\n", rank, x);
		}
		
		if (rank == (i+1) % size)
		{
			// printf("P%d waiting for P%d\n", rank, (i) % size);
			MPI_Recv(&x, 1, MPI_INT, i % size, 1, MPI_COMM_WORLD, &status);
			printf("P%d recv %d\n", rank, x);
		}	
		
	}


	MPI_Finalize();

	return 0;
}

// mpirun -np 5 ./q4.o

// Number: 8
// P0 send 8
// P1 recv 8
// P1 send 8
// P2 recv 8
// P2 send 8
// P3 recv 8
// P3 send 8
// P4 recv 8
// P4 send 8
// P0 recv 8