// 2)tiled matrix multiplication using 2D grd and 2D block

#include <stdio.h>

#define N 3
#define TILE_SIZE 2 

__global__ void matrixMul(int *a, int *b, int *c, int width) {
    __shared__ int tile_a[TILE_SIZE][TILE_SIZE];
    __shared__ int tile_b[TILE_SIZE][TILE_SIZE];

    int bx = blockIdx.x;
    int by = blockIdx.y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    int row = by * TILE_SIZE + ty;
    int col = bx * TILE_SIZE + tx;

    int value = 0;

    for (int m = 0; m < width / TILE_SIZE; ++m) {
        tile_a[ty][tx] = a[row * width + m * TILE_SIZE + tx];
        tile_b[ty][tx] = b[(m * TILE_SIZE + ty) * width + col];
        __syncthreads();

        for (int k = 0; k < TILE_SIZE; ++k) {
            value += tile_a[ty][k] * tile_b[k][tx];
        }
        __syncthreads();
    }

    c[row * width + col] = value;
}

int main() {
    int *a, *b, *c; 
    int *d_a, *d_b, *d_c; 

    a = (int *)malloc(N * N * sizeof(int));
    b = (int *)malloc(N * N * sizeof(int));
    c = (int *)malloc(N * N * sizeof(int));

    printf("Enter elements for matrix A:\n");
    for (int i = 0; i < N * N; ++i) {
        scanf("%d", &a[i]);
    }

    printf("Enter elements for matrix B:\n");
    for (int i = 0; i < N * N; ++i) {
        scanf("%d", &b[i]);
    }

    cudaMalloc(&d_a, N * N * sizeof(int));
    cudaMalloc(&d_b, N * N * sizeof(int));
    cudaMalloc(&d_c, N * N * sizeof(int));

    cudaMemcpy(d_a, a, N * N * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, N * N * sizeof(int), cudaMemcpyHostToDevice);

    dim3 threadsPerBlock(TILE_SIZE, TILE_SIZE);
    dim3 numBlocks(N / TILE_SIZE, N / TILE_SIZE);

    matrixMul<<<numBlocks, threadsPerBlock>>>(d_a, d_b, d_c, N);

    cudaMemcpy(c, d_c, N * N * sizeof(int), cudaMemcpyDeviceToHost);

    printf("Result matrix:\n");
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            printf("%d ", c[i * N + j]);
        }
        printf("\n");
    }

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    free(a);
    free(b);
    free(c);

    return 0;
}
