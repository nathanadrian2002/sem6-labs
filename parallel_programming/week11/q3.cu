// 3)WAP in CUDA to improve performance of 1D parallel convolution using constant memory

#include <stdio.h>

#define WIDTH 7
#define MASK_WIDTH 3

__constant__ int filter[MASK_WIDTH]; 

__global__ void convolution(const int *input, int *output, int width) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (tid < width) {
        int result = 0;
        for (int i = 0; i < MASK_WIDTH; ++i) {
            int input_index = tid - MASK_WIDTH / 2 + i;
            if (input_index >= 0 && input_index < width) {
                result += input[input_index] * filter[i];
            }
        }
        output[tid] = result;
    }
}

int main() {
    int input[WIDTH];
    int mask[MASK_WIDTH] = {1, 2, 3}; 
    int output[WIDTH];

    for (int i = 0; i < WIDTH; ++i) {
        input[i] = i+1;
    }

    cudaMemcpyToSymbol(filter, mask, MASK_WIDTH * sizeof(int));

    int *d_input, *d_output;
    cudaMalloc((void**)&d_input, WIDTH * sizeof(int));
    cudaMalloc((void**)&d_output, WIDTH * sizeof(int));

    cudaMemcpy(d_input, input, WIDTH * sizeof(int), cudaMemcpyHostToDevice);

    int threadsPerBlock = 256;
    int numBlocks = (WIDTH + threadsPerBlock - 1) / threadsPerBlock;

    convolution<<<numBlocks, threadsPerBlock>>>(d_input, d_output, WIDTH);

    cudaMemcpy(output, d_output, WIDTH * sizeof(int), cudaMemcpyDeviceToHost);

    printf("Convolution result:\n");
    for (int i = 0; i < WIDTH; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");

    cudaFree(d_input);
    cudaFree(d_output);

    return 0;
}
