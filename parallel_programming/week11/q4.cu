// 4)WAP in CUDA to perform 1D convolution operation on input array N of size width using mask array, M of size mask_width, to produce resultant array P of size width
 
 #include <stdio.h>

#define WIDTH 7
#define MASK_WIDTH 3

__global__ void convolution(const int *input, const int *mask, int *output, int width, int mask_width) {
    int tid_x = blockIdx.x * blockDim.x + threadIdx.x;
    int tid_y = blockIdx.y * blockDim.y + threadIdx.y;
    int tid = tid_y * width + tid_x;

    if (tid_x < width && tid_y < width) {
        int result = 0;
        for (int i = 0; i < mask_width; ++i) {
            int input_index = tid - mask_width / 2 + i;
            if (input_index >= 0 && input_index < width) {
                result += input[input_index] * mask[i];
            }
        }
        output[tid] = result;
    }
}

int main() {
    int input[WIDTH];
    int mask[MASK_WIDTH] = {1, 2, 3};
    int output[WIDTH];

    for (int i = 0; i < WIDTH ; ++i) {
        input[i] = i+1;
    }

    int *d_input, *d_mask, *d_output;
    cudaMalloc((void**)&d_input, WIDTH * sizeof(int));
    cudaMalloc((void**)&d_mask, MASK_WIDTH * sizeof(int));
    cudaMalloc((void**)&d_output, WIDTH * sizeof(int));

    cudaMemcpy(d_input, input, WIDTH * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_mask, mask, MASK_WIDTH * sizeof(int), cudaMemcpyHostToDevice);

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks((WIDTH + threadsPerBlock.x - 1) / threadsPerBlock.x, (WIDTH + threadsPerBlock.y - 1) / threadsPerBlock.y);

    convolution<<<numBlocks, threadsPerBlock>>>(d_input, d_mask, d_output, WIDTH, MASK_WIDTH);

    cudaMemcpy(output, d_output, WIDTH *  sizeof(int), cudaMemcpyDeviceToHost);

    printf("Convolution result:\n");
    for (int i = 0; i < WIDTH ; ++i) {
        printf("%d ", output[i]);
    }
    printf("\n");

    cudaFree(d_input);
    cudaFree(d_mask);
    cudaFree(d_output);

    return 0;
}
