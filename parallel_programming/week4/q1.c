
#include<stdio.h>
#include "mpi.h"

int mc = MPI_COMM_WORLD;

int main(int argc, char* argv[])
{
	int r, s, fact = 1, factsum, i;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(mc, &r);
	MPI_Comm_size(mc, &s);

	MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

	for (i = 1;i<= r+1; i++)
		fact = fact * i;
	// printf("P%d : %d\n", r, fact);

	MPI_Scan(&fact, &factsum, 1, MPI_INT, MPI_SUM, mc);

	if (r == s-1)
		printf("Sum of fact: %d\n", factsum);

	MPI_Finalize();

	return 0;

}

/*
Write an MPI program using N processes 
to find 1! + 2! + ... + 00N!. 
Use scan. Also, handle different errors using error handling routines
*/
