
/*
Write a MPI Program to read a 3x3 mat. Enter an element
to be searched in the root process. Find the number of 
occurances of this element in the mat using 3 processes. 
Also, handle different errors using error handling routines.
*/


#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int mc = MPI_COMM_WORLD;

int main(int argc, char* argv[]) {
    
    int r, s, mat[3][3], f, lc = 0, gc;
    
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(mc, &r);
    MPI_Comm_size(mc, &s);

    if (r == 0) {
        printf("Matrix:\n");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                scanf("%d", &mat[i][j]);
            }
        }
    }

    MPI_Bcast(mat, 3 * 3, MPI_INT, 0, mc);

    if (r == 0) {
        printf("Find: ");
        scanf("%d", &f);
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (mat[i][j] == f) {
                lc++;
            }
        }
    }

    MPI_Reduce(&lc, &gc, 1, MPI_INT, MPI_SUM, 0, mc);

    if (r == 0) {
        printf("No occurrences: %d\n", gc);
    }

    MPI_Finalize();
    return 0;
}
