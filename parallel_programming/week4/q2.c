
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

/*

Write a mpi program to calulcate pi value by integrating
f(x) = 4 / (1+x^2). Area under the curve is divided into
rectangles are distributed to the processors. Also handle
different errors using error handling routines.

*/

int mc = MPI_COMM_WORLD;

double f(double x) {
    return 4.0 / (1.0 + x * x);
}

int main(int argc, char* argv[]) {
    

	double a = 0, b = 1, ls = 0.0, gs, x;
    int n = 100, r, s; 
    double h = (b - a) / n;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(mc, &r);
    MPI_Comm_size(mc, &s);

    
    
    for (int i = r; i < n; i += s) {
        x = a + i * h;
        ls += h * (f(x) + f(x + h)) / 2.0;
    }

   
    MPI_Reduce(&ls, &gs, 1, MPI_DOUBLE, MPI_SUM, 0, mc);

    if (r == 0) {
        printf("Approx pi: %f\n", gs);
    }

    MPI_Finalize();
    return 0;
}
