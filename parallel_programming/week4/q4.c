

/*
Write a mpi program to read 4x4 mat and display the 
following output using 4 processes.
input:
1 2 3 4
1 2 3 1
1 1 1 1
2 1 2 1

output:
1 2 3 4
2 4 6 5
3 5 7 6
5 6 9 7

*/

#include<stdio.h>
#include "mpi.h"

int mc = MPI_COMM_WORLD;

void printMatrix(int mat[4][4]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

int main(int argc, char* argv[]) {
    
    int r, s;
    int mat[4][4];

    MPI_Init(&argc, &argv);

    
    MPI_Comm_rank(mc, &r);
    MPI_Comm_size(mc, &s);


    if (r == 0) {
        printf("Matrix:\n");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                scanf("%d", &mat[i][j]);
            }
        }
    }

    MPI_Bcast(mat, 4 * 4, MPI_INT, 0, mc);

    int row_start = r * (4 / s);
    int row_end = (r + 1) * (4 / s);

    for (int k = 0; k < 4; k++) {
        for (int i = row_start; i < row_end; i++) {
            for (int j = 0; j < 4; j++) {
                mat[i][j] += mat[i][k] * mat[k][j];
            }
        }
    }

    MPI_Barrier(mc); 

    if (r == 0) {
        printf("Output:\n");
        printMatrix(mat);
    }

    MPI_Finalize();
    return 0;
}
