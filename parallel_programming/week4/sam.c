

#include<stdio.h>
#include "mpi.h"

int mc = MPI_COMM_WORLD;

int main(int argc, char* argv[])
{
	int r, s, fact = 1, factsum, i;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(mc, &r);
	MPI_Comm_size(mc, &s);

	for (i = 1;i<= r+1; i++)
		fact = fact * i;
	printf("P%d : %d\n", r, fact);

	MPI_Reduce(&fact, &factsum, 1, MPI_INT, MPI_SUM, 0, mc);

	if (r == 0)
		printf("Sum of fact: %d\n", factsum);

	MPI_Finalize();

	return 0;

}
