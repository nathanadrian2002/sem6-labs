#include <stdio.h>
#include <cuda_runtime.h>

#define SL 100
#define WL 10
#define BS 128

__global__ void countWordKernel(char* seh, int* woh, char* wh) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    while (index < SL) {
        int i = 0;
        while (i < WL && wh[i] != '\0' && seh[index + i] == wh[i]) {
            i++;
        }
        if (i == WL || wh[i] == '\0') {
            atomicAdd(woh, 1);
        }
        index += stride;
    }
}

int main() {

    char seh[SL];
    char wh[WL];
    int woh;

    printf("Sentence: ");
    scanf("%[^\n]s",seh);
    printf("Word: ");
    scanf("%s", wh);

    char* sed;
    char* wd;
    int* wod;

    cudaMalloc(&sed, SL * sizeof(char));
    cudaMalloc(&wd, WL * sizeof(char));
    cudaMalloc(&wod, sizeof(int));
    
    cudaMemcpy(sed, seh, SL * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(wd, wh, WL * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(wod, &woh, sizeof(int), cudaMemcpyHostToDevice);
    
    countWordKernel<<<(SL + BS - 1) / BS, BS>>>(sed, wod, wd);

    cudaMemcpy(&woh, wod, sizeof(int), cudaMemcpyDeviceToHost);

    printf("Count: %d \n", woh);

    cudaFree(sed);
    cudaFree(wd);
    cudaFree(wod);

    return 0;

}