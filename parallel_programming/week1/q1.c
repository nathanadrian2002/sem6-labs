#include<mpi.h>
#include<stdio.h>
#include <stdlib.h>
#include <string.h>

int Pow(int a, int b)
{
	int result = 1;
	
	for (int i = 0; i < b; i++)
		result *= a;
	return result;
}

int main(int argc, char * argv[])
{
	int rank, size, x;

	x = atoi(argv[1]);

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Comm_size(MPI_COMM_WORLD, &size);


	// printf("My rank is %d in total %d processes\n", rank, size);

	printf("Pow(%d, %d) = %d\n", x, rank, Pow(x, rank));

	MPI_Finalize();

	return 0;

}