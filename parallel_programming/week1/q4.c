#include<mpi.h>
#include<stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char * argv[])
{

	int rank, size;
	char str[50];

	strcpy(str, argv[1]);


	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Comm_size(MPI_COMM_WORLD, &size);


	if (rank < strlen(str))
		str[rank] ^= ' ';
	

	printf("String(%d): %s\n", rank, str);

	MPI_Finalize();



	return 0;

}