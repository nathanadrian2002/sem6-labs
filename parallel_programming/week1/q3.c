#include<mpi.h>
#include<stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char * argv[])
{
	int rank, size, a, b;

	a = atoi(argv[1]);
	b = atoi(argv[2]);

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Comm_size(MPI_COMM_WORLD, &size);


	switch(rank)
	{
		case 0:
			printf("%d + %d = %d\n", a, b, a+b);
			break;
		case 1:
			printf("%d - %d = %d\n", a, b, a-b);
			break;
		case 2:
			printf("%d * %d = %d\n", a, b, a*b);
			break;
		case 3:
			printf("%d / %d = %d\n", a, b, a/b);
			break;
		default:
			printf("Excess Processes\n");
	}


	MPI_Finalize();

	return 0;

}